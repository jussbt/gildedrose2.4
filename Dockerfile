FROM openjdk:8-jre-alpine
COPY   target/gildedRose-0.0.1-SNAPSHOT.jar /usr/src/gildedrose/
WORKDIR /usr/src/gildedrose
EXPOSE 8080:8080
CMD ["java", "-jar", "gildedRose-0.0.1-SNAPSHOT.jar"]

