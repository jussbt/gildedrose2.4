package com.telesoftas.gildedRose.job;

import com.google.gson.Gson;
import com.telesoftas.gildedRose.entity.*;
import com.telesoftas.gildedRose.repository.ItemRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class ScheduleJob {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduleJob.class);

    @Autowired
    ItemRepo itemRepo;

    /**
     * 8 AM every day.
     */
    @Scheduled(cron = "0 0 8 * * *")
    public void updateProducts() {
        LOG.info("JoB updateProducts is running...");
        itemRepo.findAll().spliterator()
                .forEachRemaining((item -> {
                    item = castItemsIntoSubclasses(item);
                    item.updateQuality();
                    itemRepo.save(item);
                    LOG.info("Item {} was updated", item.name);
                }));
    }

    public Item castItemsIntoSubclasses(Item item) {
        Gson gson = new Gson();
        if (item.name.contains("Conjured")) {
            return gson.fromJson(gson.toJson(item), Conjured.class);
        }
        switch (item.name) {
            case "Aged Brie":
                return gson.fromJson(gson.toJson(item), AgedBrie.class);
            case "Sulfuras, Hand of Ragnaros":
                return gson.fromJson(gson.toJson(item), SulfurusItem.class);
            case "Backstage passes to a TAFKAL80ETC concert":
                return gson.fromJson(gson.toJson(item), BackstageItem.class);
            default:
                return item;
        }
    }

    /**
     * fill DB if it is empty.
     */
    @Scheduled(fixedDelay = 1000 * 60 * 60)
    public void insertDataIntoDatabaseIfEmpty() {
        if (!itemRepo.findAll().iterator().hasNext()) {
            List<Item> items = Arrays.asList(
                    new Item("+5 Dexterity Vest", 10, 20), //
                    new AgedBrie(2, 0), //
                    new AgedBrie(5, 7), //
                    new SulfurusItem(0), //
                    new SulfurusItem(-1),
                    new BackstageItem(15, 20),
                    new BackstageItem(10, 49),
                    new BackstageItem(5, 49),
                    // this conjured item does not work properly yet
                    new Conjured("Conjured Mana Cake", 3, 6));
            itemRepo.save(items);
        }
    }
}
