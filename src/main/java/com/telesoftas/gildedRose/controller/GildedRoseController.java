package com.telesoftas.gildedRose.controller;

import com.google.gson.Gson;
import com.telesoftas.gildedRose.entity.Item;
import com.telesoftas.gildedRose.repository.ItemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RestController
public class GildedRoseController {
    @Autowired
    ItemRepo itemRepo;

    /**
     * I decided that return all objects faster > faster update.That is why I save different all items  with same indexes.
     * Gson is the fastest converter for small Json files(<10mb);
     */
    @GetMapping(value = "/api/all")
    public String findAll() {
        Gson gson = new Gson();
        List<Item> items = new ArrayList<>();
        itemRepo.findAll().forEach(items::add);
        items.sort(Comparator.comparing(item -> item.name));
        return gson.toJson(items);
    }
}
