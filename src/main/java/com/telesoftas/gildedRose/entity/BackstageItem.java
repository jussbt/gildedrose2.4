package com.telesoftas.gildedRose.entity;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "rose", type = "item")
public class BackstageItem extends Item {
    private final byte stage1 = 10;
    private final byte stage2 = 5;

    public BackstageItem(int sellIn, int quality) {
        super("Backstage passes to a TAFKAL80ETC concert", sellIn, quality);
    }

    @Override
    public void updateQuality() {
        sellIn--;
        quality++;
        if (sellIn < 0) {
            quality = 0;
            return;
        }
        if (sellIn < stage1) {
            quality++;
        }
        if (sellIn < stage2) {
            quality++;
        }
        quality = quality > MAXIMUM_QUALITY ? MAXIMUM_QUALITY : quality;
    }
}
