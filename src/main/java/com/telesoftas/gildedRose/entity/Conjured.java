package com.telesoftas.gildedRose.entity;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "rose", type = "item")
public class Conjured extends Item {
    public Conjured(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
        sellIn--;
        quality -= 2;
        if (sellIn < 0) {
            quality -= 2;
        }
        quality = quality < 0 ? 0 : quality;
    }
}
