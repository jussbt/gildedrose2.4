package com.telesoftas.gildedRose.entity;

import org.springframework.data.elasticsearch.annotations.Document;

import java.util.UUID;

/**
 * I decided that return all objects is more important than faster update.That is why I save different items  with same indexes.
 */
@Document(indexName = "rose", type = "item")
public class Item {
    transient final int MAXIMUM_QUALITY = 50;
    public String name;
    public int sellIn;
    public int quality;
    public String id = UUID.randomUUID().toString();

    public Item() {
    }

    public Item(String name, int sellIn, int quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }

    public void updateQuality() {
        sellIn--;
        quality--;
        if (sellIn < 0) {
            quality--;
        }
        quality = quality < 0 ? 0 : quality;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.sellIn + ", " + this.quality;
    }
}
