package com.telesoftas.gildedRose.entity;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "rose", type = "item")
public class SulfurusItem extends Item {
    /**
     * Quality always 80!
     */
    public SulfurusItem(int sellIn) {
        super("Sulfuras, Hand of Ragnaros", sellIn, 80);
    }

    @Override
    public void updateQuality() {
        //does nothing , just overrides;
    }
}
