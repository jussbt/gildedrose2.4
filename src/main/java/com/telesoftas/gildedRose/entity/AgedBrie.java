package com.telesoftas.gildedRose.entity;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "agedbrie", type = "item")
public class AgedBrie extends Item {
    public AgedBrie() {
    }

    public AgedBrie(int sellIn, int quality) {
        super("Aged Brie", sellIn, quality);
    }

    @Override
    public void updateQuality() {
        sellIn--;
        quality++;
        if (sellIn < 0) {
            quality++;
        }
        quality = quality > MAXIMUM_QUALITY ? MAXIMUM_QUALITY : quality;
    }
}
