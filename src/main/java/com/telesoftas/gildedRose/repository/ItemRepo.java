package com.telesoftas.gildedRose.repository;

import com.telesoftas.gildedRose.entity.Item;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepo extends ElasticsearchRepository<Item, String> {
}
