package com.telesoftas.gildedRose.services;

import com.telesoftas.gildedRose.entity.Item;
import com.telesoftas.gildedRose.job.ScheduleJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class GildedRose {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduleJob.class);

    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public Item[] getItems() {
        return items;
    }

    public void updateQuality() {
        List<Item> items = Arrays.asList(getItems());
        items.parallelStream().forEach(item -> {
            item.updateQuality();
            LOG.info(" {}  {} was updated . Thread : {}", item.id, item.name, Thread.currentThread().getId());
        });
    }
}