package com.telesoftas.gildedRose.jobs;

import com.telesoftas.gildedRose.entity.*;
import com.telesoftas.gildedRose.job.ScheduleJob;
import com.telesoftas.gildedRose.repository.ItemRepo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Tests for ScheduleJob class
 */
@RunWith(SpringRunner.class)
public class ScheduledJobTest {

    @Mock
    ItemRepo itemRepo;
    @InjectMocks
    ScheduleJob scheduleJob;

    @Test
    public void testUpdateJobWithNoItemsInDB() {
        when(itemRepo.findAll()).thenReturn(Collections.EMPTY_LIST);
        scheduleJob.updateProducts();
        verify(itemRepo, times(0)).save((Item) any());
    }

    @Test
    public void testInsertIntoDatabaseJobWhenDbIsEmpty() {
        when(itemRepo.findAll()).thenReturn(Collections.emptyList());
        scheduleJob.insertDataIntoDatabaseIfEmpty();
        verify(itemRepo, times(1)).save((anyList()));
    }

    @Test
    public void testInsertIntoDatabaseWhenDbIsNotEmpty() {
        when(itemRepo.findAll()).thenReturn(Arrays.asList(new SulfurusItem(5)));
        scheduleJob.insertDataIntoDatabaseIfEmpty();
        verify(itemRepo, times(0)).save((anyList()));
    }

    @Test
    public void testUpdateJobWithOneItem() {
        when(itemRepo.findAll()).thenReturn(Arrays.asList(new SulfurusItem(5)));
        scheduleJob.updateProducts();
        verify(itemRepo, times(1)).save((Item) any());
    }

    @Test
    public void testUpdateJobWithFiveProducts() {
        List<Item> items = Arrays.asList(new SulfurusItem(1), new BackstageItem(1, 1), new AgedBrie(0, 99), new AgedBrie(5, 5), new Conjured("Conjured", 5, 4));
        when(itemRepo.findAll()).thenReturn(items);
        scheduleJob.updateProducts();
        verify(itemRepo, times(5)).save((Item) any());
    }

    @Test
    public void testCastItemIntoSulfuras() {
        Item item = new Item("Sulfuras, Hand of Ragnaros", 50, 50);
        item = scheduleJob.castItemsIntoSubclasses(item);
        Assert.assertSame(item.getClass(), SulfurusItem.class);
    }

    @Test
    public void testCastItemIntoAgedBrie() {
        Item item = new Item("Aged Brie", 50, 50);
        item = scheduleJob.castItemsIntoSubclasses(item);
        Assert.assertSame(item.getClass(), AgedBrie.class);
    }

    @Test
    public void testCastItemIntoBackStage() {
        Item item = new Item("Backstage passes to a TAFKAL80ETC concert", 50, 50);
        item = scheduleJob.castItemsIntoSubclasses(item);
        Assert.assertSame(item.getClass(), BackstageItem.class);
    }

    @Test
    public void testCastItemIntoConjured() {
        Item item = new Item("ConjuredTest", 50, 50);
        item = scheduleJob.castItemsIntoSubclasses(item);
        Assert.assertSame(item.getClass(), Conjured.class);
    }

    @Test
    public void testDefault() {
        Item item = new Item(" Simple Item", 50, 50);
        item = scheduleJob.castItemsIntoSubclasses(item);
        Assert.assertSame(item.getClass(), Item.class);
    }
}
