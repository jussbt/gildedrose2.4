package com.telesoftas.gildedRose.controller;

import com.google.gson.Gson;
import com.telesoftas.gildedRose.entity.Item;
import com.telesoftas.gildedRose.repository.ItemRepo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class GildedRoseControllerTest {
    @Mock
    ItemRepo itemRepo;
    @InjectMocks
    GildedRoseController gildedRoseController;
    Gson gson = new Gson();

    @Test
    public void giveBackEmptyList() {
        when(itemRepo.findAll()).thenReturn(Collections.singletonList(null));
        String answer = gildedRoseController.findAll();
        Assert.assertTrue(answer.equals(gson.toJson(Collections.singleton(null))));
        verify(itemRepo, times(1)).findAll();
    }

    @Test
    public void returnListWithFewObjects() {
        Item item1 = new Item("Item1", 50, 50);
        Item item2 = new Item("Item2", 50, 50);
        when(itemRepo.findAll()).thenReturn(Arrays.asList(item1, item2));
        List<Item> items = Arrays.asList(gson.fromJson(gildedRoseController.findAll(), Item[].class));
        verify(itemRepo, times(1)).findAll();
        Assert.assertTrue(items.stream().allMatch(item -> item.toString().equals(item1.toString()) || item.toString().equals(item2.toString())));
    }
}
