package com.telesoftas.gildedRose.service;

import com.telesoftas.gildedRose.entity.*;
import com.telesoftas.gildedRose.services.GildedRose;
import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for Gilded Rose update method for for every object.
 */
public class GildedRoseServiceOneObjectTest {
    GildedRose gildedRose;

    @Test//Simple case : quality ++ , sellIn --
    public void simpleItemUpdateTestMinimumBound() {
        Item item = new Item("simple Item", 99, 0);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(0, gildedRose.getItems()[0].quality);
        Assert.assertEquals(98, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("simple Item", gildedRose.getItems()[0].name);
    }

    @Test
    public void testAgedBrieSimpleCase() {
        Item item = new AgedBrie(1, 0);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(1, gildedRose.getItems()[0].quality);
        Assert.assertEquals(0, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Aged Brie", gildedRose.getItems()[0].name);
    }

    @Test//Quality is already maximum - so it will not change.
    public void testAgedBrieWithMaximumQuality() {
        Item item = new AgedBrie(1, 50);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(50, gildedRose.getItems()[0].quality);
        Assert.assertEquals(0, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Aged Brie", gildedRose.getItems()[0].name);
    }

    @Test//sellIn should be negative and quality increase by 2
    public void testAgedBrieWithNegativeSellIn() {
        Item item = new AgedBrie(0, 0);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(2, gildedRose.getItems()[0].quality);
        Assert.assertEquals(-1, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Aged Brie", gildedRose.getItems()[0].name);
    }

    @Test//Hand of Ragnaros values can not change.
    public void tryToTestSulfurasAverageCase() {
        Item item = new SulfurusItem(75);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(80, gildedRose.getItems()[0].quality);
        Assert.assertEquals(75, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Sulfuras, Hand of Ragnaros", gildedRose.getItems()[0].name);
    }

    @Test//Create Sulfuras with quality >80. After update it should be 80!
    public void tryToTestSulfurasMaximumQuality() {
        Item item = new SulfurusItem(-1);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(80, gildedRose.getItems()[0].quality);
        Assert.assertEquals(-1, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Sulfuras, Hand of Ragnaros", gildedRose.getItems()[0].name);
    }

    @Test
    public void tryToUpdateBackstagePassesAverageCase() {
        Item item = new BackstageItem(15, 20);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(21, gildedRose.getItems()[0].quality);
        Assert.assertEquals(14, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Backstage passes to a TAFKAL80ETC concert", gildedRose.getItems()[0].name);
    }

    @Test
    public void tryToUpdateBackstageWhen10DaysLeft() {
        Item item = new BackstageItem(10, 20);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(22, gildedRose.getItems()[0].quality);
        Assert.assertEquals(9, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Backstage passes to a TAFKAL80ETC concert", gildedRose.getItems()[0].name);
    }

    @Test
    public void tryToUpdateBackstagePassesWhen5DaysLeft() {
        Item item = new BackstageItem(5, 20);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(23, gildedRose.getItems()[0].quality);
        Assert.assertEquals(4, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Backstage passes to a TAFKAL80ETC concert", gildedRose.getItems()[0].name);
    }

    @Test
    public void tryToUpdateBackstagePassesWhen1DayLeftAndQualityAlmostMaximum() {
        Item item = new BackstageItem(1, 48);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(50, gildedRose.getItems()[0].quality);
        Assert.assertEquals(0, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Backstage passes to a TAFKAL80ETC concert", gildedRose.getItems()[0].name);
    }

    @Test//Quality should become 0;
    public void tryToUpdateBackstagePassesAfterConcert() {
        Item item = new BackstageItem(0, 50);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(0, gildedRose.getItems()[0].quality);
        Assert.assertEquals(-1, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Backstage passes to a TAFKAL80ETC concert", gildedRose.getItems()[0].name);
    }

    @Test
    public void tryToUpdateConjuringItemsSimpleCase() {
        Item item = new Conjured("Conjure Vine", 2, 30);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(28, gildedRose.getItems()[0].quality);
        Assert.assertEquals(1, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Conjure Vine", gildedRose.getItems()[0].name);
    }

    @Test
    public void tryToUpdateConjuringItemsWhenSellInIsNegative() {
        Item item = new Conjured("Conjure Vine", 0, 30);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(26, gildedRose.getItems()[0].quality);
        Assert.assertEquals(-1, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Conjure Vine", gildedRose.getItems()[0].name);
    }

    @Test
    public void tryToUpdateConjuringItemsWhenSellInIsNegativeAndQualityIs0() {
        Item item = new Conjured("Conjure Vine", 0, 0);
        gildedRose = new GildedRose(new Item[]{item});
        gildedRose.updateQuality();
        Assert.assertEquals(0, gildedRose.getItems()[0].quality);
        Assert.assertEquals(-1, gildedRose.getItems()[0].sellIn);
        Assert.assertEquals("Conjure Vine", gildedRose.getItems()[0].name);
    }
}
