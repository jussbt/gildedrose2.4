package com.telesoftas.gildedRose.service;

import com.google.gson.Gson;
import com.telesoftas.gildedRose.entity.AgedBrie;
import com.telesoftas.gildedRose.entity.BackstageItem;
import com.telesoftas.gildedRose.entity.Item;
import com.telesoftas.gildedRose.entity.SulfurusItem;
import com.telesoftas.gildedRose.services.GildedRose;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Tests for  GildedRose update service with many objects.
 */
public class GildedRoseServiceManyDataTest {
    private final List<Item> testingData = Arrays.asList(
            new Item("+5 Dexterity Vest", 10, 20),
            new AgedBrie(2, 7),
            new AgedBrie(5, 0),
            new SulfurusItem(0),
            new SulfurusItem(-1),
            new BackstageItem(15, 20),
            new BackstageItem(10, 45),
            new BackstageItem(5, 30),
            new BackstageItem(3, 6));
    Gson gson = new Gson();
    private List<Item> updateThisList = Arrays.asList(
            new Item("+5 Dexterity Vest", 10, 20),
            new AgedBrie(2, 7),
            new AgedBrie(5, 0),
            new SulfurusItem(0),
            new SulfurusItem(-1),
            new BackstageItem(15, 20),
            new BackstageItem(10, 45),
            new BackstageItem(5, 30),
            new BackstageItem(3, 6));

    @Before
    public void updateList() {
        GildedRose gildedRose = new GildedRose(updateThisList.toArray(new Item[0]));
        gildedRose.updateQuality();
    }

    @Test
    public void testAgedBrie() {
        List<Item> agedBrieBeforeUpdate = testingData.stream().filter(item -> item.name.equals("Aged Brie")).sorted(Comparator.comparing(item -> item.quality)).collect(Collectors.toList());
        List<Item> agedBrieAfterUpdate = updateThisList.stream().filter(item -> item.name.equals("Aged Brie")).sorted(Comparator.comparing(item -> item.quality)).collect(Collectors.toList());
        Assert.assertEquals(agedBrieBeforeUpdate.size(), agedBrieAfterUpdate.size());
        for (int i = 0; i < agedBrieAfterUpdate.size(); i++) {
            Assert.assertEquals(agedBrieAfterUpdate.get(i).name, agedBrieBeforeUpdate.get(i).name);
            Assert.assertEquals(agedBrieAfterUpdate.get(i).sellIn + 1, agedBrieBeforeUpdate.get(i).sellIn);
            Assert.assertEquals(agedBrieAfterUpdate.get(i).quality - 1, agedBrieBeforeUpdate.get(i).quality);
        }
    }

    @Test
    public void testRagnaros() {
        List<Item> agedBrieBeforeUpdate = testingData.stream().filter(item -> item.name.contains("Sulfuras")).sorted(Comparator.comparing(item -> item.quality)).collect(Collectors.toList());
        List<Item> agedBrieAfterUpdate = updateThisList.stream().filter(item -> item.name.contains("Sulfuras")).sorted(Comparator.comparing(item -> item.quality)).collect(Collectors.toList());
        Assert.assertEquals(agedBrieBeforeUpdate.size(), agedBrieAfterUpdate.size());
        for (int i = 0; i < agedBrieAfterUpdate.size(); i++) {
            Assert.assertEquals(agedBrieAfterUpdate.get(i).name, agedBrieBeforeUpdate.get(i).name);
            Assert.assertEquals(agedBrieAfterUpdate.get(i).sellIn, agedBrieBeforeUpdate.get(i).sellIn);
            Assert.assertEquals(agedBrieAfterUpdate.get(i).quality, agedBrieBeforeUpdate.get(i).quality);
        }
    }

    @Test
    public void testBackStagePasses() {
        List<Item> agedBrieBeforeUpdate = testingData.stream().filter(item -> item.name.contains("Backstage")).sorted(Comparator.comparing(item -> item.quality)).collect(Collectors.toList());
        List<Item> agedBrieAfterUpdate = updateThisList.stream().filter(item -> item.name.contains("Backstage")).sorted(Comparator.comparing(item -> item.quality)).collect(Collectors.toList());
        System.out.println(gson.toJson(agedBrieBeforeUpdate));
        System.out.println(gson.toJson(agedBrieAfterUpdate));
        Assert.assertEquals(agedBrieBeforeUpdate.size(), agedBrieAfterUpdate.size());
        for (int i = 0; i < agedBrieAfterUpdate.size(); i++) {
            if (agedBrieBeforeUpdate.get(i).sellIn > 10) {
                Assert.assertEquals(agedBrieBeforeUpdate.get(i).quality + 1, agedBrieAfterUpdate.get(i).quality);
            } else if (agedBrieBeforeUpdate.get(i).sellIn > 5) {
                Assert.assertEquals(agedBrieBeforeUpdate.get(i).quality + 2, agedBrieAfterUpdate.get(i).quality);
            } else if (agedBrieBeforeUpdate.get(i).sellIn > 0) {
                Assert.assertEquals(agedBrieBeforeUpdate.get(i).quality + 3, agedBrieAfterUpdate.get(i).quality);
            } else {
                Assert.assertEquals(0, agedBrieAfterUpdate.get(i).quality);
            }
        }
    }

    @Test
    public void saveUpdatedProductsIntoFileRefractoredMethod() throws IOException {
        GildedRose gildedRose = new GildedRose(testingData.toArray(new Item[0]));
        FileWriter fileWriter = new FileWriter("src/test/java/resources/GildedRefactoredData.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);
        for (int days = 0; days <= 20; days++) {
            String separator = "-------- day" + days + "--------";
            String parameters = "name , quality , sellIn";
            printWriter.println(separator);
            printWriter.println(parameters);
            for (int i = 0; i < gildedRose.getItems().length; i++) {
                printWriter.println(gildedRose.getItems()[i].toString());
            }
            gildedRose.updateQuality();
        }
        printWriter.close();
        Assert.assertTrue(compareTwoFiles(new File("src/test/java/resources/GildedRefactoredData.txt"), new File("src/test/java/resources/GildedNotRefactoredData.txt")));
    }

    private boolean compareTwoFiles(File file1, File file2) throws IOException {
        return FileUtils.contentEquals(file1, file2);
    }
}
